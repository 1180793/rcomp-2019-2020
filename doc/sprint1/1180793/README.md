RCOMP 2019-2020 Project - Sprint 1 - Member 1180793 folder
===========================================

# Building B

The building B horizontal dimensions are, approximately, 60 x 20 meters.

## Floor Zero

Room **B0.1** is a storage area and network outlets are __NOT__ required there, the same applies to restrooms.

#### Measurements
![Building B - Floor Zero Measurements](BuildingB-FloorZero_Measurements.png)

* **B0.1** - 21.6 m2
* **B0.2** - 92.2 m2
* **B0.3** - 131.1 m2
* **B0.4** - 157.5 m2

#### Outlet Layout
![Building B - Floor Zero Outlet Layout](BuildingB-FloorZero_Outlets.png)


## Floor One

#### Measurements
![Building B - Floor One Measurements](BuildingB-FloorOne_Measurements.png)

* **B1.1** - 21.6 m2
* **B1.2** - 37.0 m2
* **B1.3** - 41.8 m2
* **B1.4** - 41.8 m2
* **B1.5** - 41.8 m2
* **B1.6** - 47.3 m2
* **B1.7** - 47.3 m2
* **B1.8** - 47.3 m2
* **B1.9** - 81.4 m2
* **B1.10** - 55.5 m2
* **B1.11** - 55.5 m2

#### Outlet Layout
![Building B - Floor One Outlet Layout](BuildingB-FloorOne_Outlets.png)

## Access Points

![Building B - Floor Zero APs](BuildingB-FloorZero_APs.png)

![Building B - Floor One APs](BuildingB-FloorOne_APs.png)

Os AP estarão todos em canais diferentes para prevenir interferências.

Foram escolhidos AP de 2.4GHz, uma vez que é necessário a rede atravessar paredes e teto/chão, sendo que esta frequência atravessa objetos melhor que a de 5GHz.

## Cable Paths
Os cabos no piso inferior irão passar por baixo do chão, no circuito já existente. No caso do piso superior os cabos estarão por cima do teto falso.

Os cabos que ligam os CP aos outlets estarão pelo chão ou em rodapés, devidamente isolados e protegidos.

De cada HC sairá um cabo de fibra ótica para cada CP, porque se apenas existisse um cabo a passar por todos os CP e esse mesmo cabo avariasse iria afetar o resto da rede. Desta maneira caso um cabo avarie apenas irá afetar o CP e a sala correspondentes.

## Inventory

* Outlets: 192
Note: the values for cables are approximated

* Optical Cable: 600 m

* Copper Cable: 3100 m

* 1 Intermediate cross-connect

* 2 Horizontal cross-connects

* 6 Access Points

* 5 Consolidation Points

Os patch panels utilizados nos HC irão receber um cabo de fibra ótica e ter como saída os de cobre. Serão utilizados patch panels de 24 conexões.
Por fim, são necessários 16 cabos extra de fibra �tica monomode de 6 fibras para fazer as ligações dos patch panels de fibra ótica para os switches, nos CP, IC ,HC. Ainda são precisos 240 cabos extra de cobre CAT 7, para ligar dos switches aos patch panels de cobre, nos CP.
