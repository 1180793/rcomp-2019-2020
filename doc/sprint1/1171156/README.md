RCOMP 2019-2020 Project - Sprint 1 - Member 1171156 folder
===========================================

# Campus Backbone:

A technical ditch é percorrida por fibra ótica multimodal,sendo que estamos a num contexto de distâncias a percorrer na ordem das centenas de metros e não dos milhares, não havendo assim problemas de dispersão que tornem necessário o recurso a fibra ótica monomodal.
A fibra ótica ao entrar no campus passa primeiramente pelo edifício A onde se encontra o Main Cross Connect, apenas após passar aqui a ligação é dispersa pelo edifício A e percorre redundantemente o perímetro do campus estabelecendo conexão com todos os edifícios. Em seguida, um adaptador fibra ótica para cabo de cobre é necessário por cada edifício inserindo-se entre o Intermediate Cross Connect e o Horizontal Cross Connect, deste modo assegurando a facilidade de acesso para os utilizadores da rede.

![CampusBackBone-Redundante](Campus_Redundant.png)

# Building A:

#### Dimensões

O Edifício A apresenta dimensões de 38.26 x 19.56 metros(aproximadamente 40 x 20 metros), tendo o piso zero 3 metros de altura e o primeiro 4. Devido á baixa densidade de outlets e pequenas dimensões desta infraestrutura, sendo que nenhum cabo percorre 80 metros apartir do Horizontal Cross Connect do piso 1 e este cobre uma área inferior a 1000 m2(800m2), deste modo esse será suficiente para assegurar uma conexão estável a todos os utilizadores deste departamento evitando simultaneamente gastos desnecessários.
Em todas as salas com exceção da A0.3, A1.1 e A1.5 o planeamento dos outlets de rede foi realizado em conformidade com a norma de 2 outlets por cada 10m2. Foram ainda incluidos no planeamento 2 AP de 2.4GHz, tendo em conta a neccessidade de atravessar obstáculos como paredes e outros objetos, em deterimento de 5GHz.


## Piso Zero
* **A0.1** - 82 m2
* **A0.2** - 129 m2
* **A0.3** - 495 m2

![Building A - Floor Zero Outlet Layout](BA_F0_outlets.png)

## Piso 1
* **A1.1** - 230 m2
* **A1.2** - 45 m2
* **A1.3** - 84 m2
* **A1.4** - 130 m2
* **A1.5** - 24 m2

![Building A - Floor One Outlet Layout](BA_F1_outlet.png)

## Inventário

* **Outlets:**

**Piso 0:** 
1 x Placa de 5 Entradas.

10 x Placa de 4 Entradas .

**Piso 1:**
2 x Placa de 2 Entradas .

12 x Placa de 4 Entradas.

* **Conectores:**

97 x Conector Fêmea ISO 8877 .

388 x Conector Macho ISO 8877.

* **Patch Panels:** 

3 x Patch Panel 48 Entradas.

1 x Patch Panel Fibra Ótica.

* **Patch Cords :** 

194 x Cabo de Cobre 0.5 m.

97 x Cabo de Cobre 5 m.

**Comprimento Fio de Cobre:** 2870 metros.

* **Dimensão Bastidor Piso 1:**

(3 x Patch Panel 48 Entradas) = 3 x 2 U = 6 U.

(1 x Patch Panel Fibra Ótica) = 2 U.

**Com margem para expansão:** (6 + 2) x 4 = 32 U.


