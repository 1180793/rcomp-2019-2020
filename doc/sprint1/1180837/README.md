RCOMP 2019-2020 Project - Sprint 1 - Member 1180737 folder

# Building C 
The building C horizontal dimensions are approximately, 80*30 meters.

## Floor 0

### Measurements
![Building C - Floor Zero Measurements](BuildingC_GroundFloor_Measurements.png)

* **C0.1** - 45 m2
* **C0.2** - 45 m2
* **C0.3** - 45 m2
* **C0.4** - 46 m2
* **C0.5** - 35 m2

#### Outlet Layout
![Building C - Ground Floor Outlet Layout](BuildingC_GroundFloor_Outlets.png)

Foi usada a norma de 2 outlets por 10 m2, foi ainda adicionada uma outlet extra destinada à implementaçao de Wi-Fi.

### Cable Pathways

![Building C - Floor Zero Cable Pathway](BuildingC_GroundFloor_CablePathways.png)

### Wi-Fi Coverage
![Building C - Floor Zero Wi-Fi Coverage ](BuildingC_GroundFloor_Wi-Fi.png)

## First Floor

### Measurements

![Building C - First Floor Measurements](BuildingC_FirstFloor_Measurements.png)

* **C1.1** - 45 m2
* **C1.2** - 45 m2
* **C1.3** - 66 m2
* **C1.4** - 82 m2
* **C1.5** - 71 m2

#### Outlet Layout
![Building C - First Floor Outlet Layout](BuildingC_FirstFloor_Outlet.png)

Foi usada a norma de 2 outlets por 10 m2, foi ainda adicionada uma outlet extra destinada à implementaçao de Wi-Fi na sala C1.4

### Cable Pathways
![Building C - First Floor Cable Pathways ](BuildingC_FirstFloor_CablePathways.png)

### Wi-Fi Coverage
![Building C - First Floor Wi-Fi Coverage ](BuildingC_FirstFloor_Wi-Fi.png)



## Inventory

* Outlets: 112
 Note: the values for cables are approximated

* Optical Cable: 500 m

* Copper Cable: 2000 m

* 1 Intermediate cross-connect

* 2 Horizontal cross-connects

* 2 Access Points

* 2 Consolidation Points
