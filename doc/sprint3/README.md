﻿RCOMP 2019-2020 Project - Sprint 2 - Member 1171156 folder
==========================================================================================================================================================================================

# Step 1: OSPF

###Building A: (Area 1)
* router ospf 100
* network 10.166.199.1 255.255.254.0 area 0
* network 10.166.192.0 255.255.254.0 area 1

###Building B: (Area 2)
* router ospf 200
* network 10.166.199.1 255.255.254.0 area 0
* network 10.166.194.0 255.255.254.0 area 2

###Building C1: (Area 3) MC Furtas
* router ospf 300
* network 10.166.199.1 255.255.254.0 area 0
* network 10.166.196.0 255.255.254.0 area 3

###Building C2: (Area 4) Gilaço
* router ospf 400
* network 10.166.199.1 255.255.254.0 area 0
* network 10.166.200.0 255.255.254.0 area 4


# Step 2: Creating DMZ servers for each Building

###Building A:
 * HTTPS server IP:  10.166.193.2
 * HTTPS server mask : 255.255.255.128
 * HTTPS Gateway : 10.166.193.72


###Building B:
* HTTPS server IP:  10.166.195.123
* HTTPS server mask : 255.255.255.240
* HTTPS Gateway : 10.166.195.133

###Building C1:  
* HTTPS server IP: 10.166.197.2
* HTTPS server mask : 255.255.255.0
* HTTPS Gateway : 10.166.197.252

###Building C2:
* HTTPS server IP: 10.166.201.2
* HTTPS server mask : 255.255.255.0
* HTTPS Gateway : 10.166.200.253





# Step 3: DHCPv4service & DNS

###Building A:

######### Excluir os ips ja atribuidos:
* Router(config)#ip dhcp excluded-address 10.166.192.42
* Router(config)#ip dhcp excluded-address 10.166.192.122
* Router(config)#ip dhcp excluded-address 10.166.192.235
* Router(config)#ip dhcp excluded-address 10.166.193.72
------------------------------------------------------------
* ip dhcp pool FirstFloorAUsers (vlan 310)
* network 10.166.192.1 255.255.255.192
* default-router 10.166.192.42
* domain-name rcomp-19-20-df-g2
* dns-server 10.166.193.1
------------------------------------------------------------
* ip dhcp pool GroundFloorAUsers (vlan 311)
* network 10.166.192.80 255.255.255.192
* default-router 10.166.192.122
* domain-name rcomp-19-20-df-g2
* dns-server 10.166.193.1
------------------------------------------------------------
* ip dhcp pool WiFi-AUsers (vlan 312)
* network 10.166.192.210 255.255.255.254
* default-router 10.166.192.235
* domain-name rcomp-19-20-df-g2
* dns-server 10.166.193.1
------------------------------------------------------------
* ip dhcp pool DMZ-AUsers (vlan 313)
* network 10.166.193.1 255.255.255.128
* default-router 10.166.193.72
* domain-name rcomp-19-20-df-g2
* dns-server 10.166.193.1


###Building B:
######### Excluir os ips ja atribuidos:
* Router(config)#ip dhcp excluded-address 10.166.194.62
* Router(config)#ip dhcp excluded-address 10.166.194.146
* Router(config)#ip dhcp excluded-address 10.166.195.102
* Router(config)#ip dhcp excluded-address 10.166.195.133
------------------------------------------------------------

* ip dhcp pool GroundFloorBUsers (vlan 335)
* network 10.166.194.1 255.255.255.192
* default-router 10.166.194.62
* domain-name building-b.rcomp-19-20-df-g2
* dns-server 10.166.195.133
------------------------------------------------------------
* ip dhcp pool FirstFloorBUsers(vlan 336)
* network 10.166.194.75 255.255.255.128
* default-router 10.166.194.146
* domain-name building-b.rcomp-19-20-df-g2
* dns-server 10.166.195.133

------------------------------------------------------------
* ip dhcp pool WiFiBUsers(vlan 337)
* network 10.166.195.1 255.255.255.128
* default-router 10.166.195.102
* domain-name building-b.rcomp-19-20-df-g2
* dns-server 10.166.195.133
------------------------------------------------------------
* ip dhcp pool DMZ_BUsers (vlan 338)
* network 10.166.195.122 255.255.255.240
* default-router 10.166.195.133
* domain-name building-b.rcomp-19-20-df-g2
* dns-server 10.166.195.133


###Building C1:
######### Excluir os ips ja atribuidos:
* Router(config)#ip dhcp excluded-address 10.166.196.110
* Router(config)#ip dhcp excluded-address 10.166.196.41
* Router(config)#ip dhcp excluded-address 10.166.196.190
* Router(config)#ip dhcp excluded-address 10.166.197.252
------------------------------------------------------------

* ip dhcp pool GroundFloorCUsers (vlan 346)
* Router(dhcp-config)#network 10.166.196.1 255.255.255.192
* Router(dhcp-config)#default-router 10.166.196.41
* Router(dhcp-config)#domain-name building-c.rcomp-19-20-df-g2
* Router(dhcp-config)#dns-server 10.166.197.252
------------------------------------------------------------

* Router(config)#ip dhcp pool FirstFloorCUsers (vlan 345)
* Router(dhcp-config)#network 10.166.196.66 255.255.255.192
* Router(dhcp-config)#default-router 10.166.196.110
* Router(dhcp-config)#domain-name building-c.rcomp-19-20-df-g2
* Router(dhcp-config)#dns-server 10.166.197.252
------------------------------------------------------------
* Router(config)#ip dhcp pool WiFiCUsers (vlan 347)
* Router(dhcp-config)#network 10.166.196.129 255.255.255.192
* Router(dhcp-config)#default-router 10.166.196.190
* Router(dhcp-config)#domain-name building-c.rcomp-19-20-df-g2
* Router(dhcp-config)#dns-server 10.166.197.252
------------------------------------------------------------
* Router(config)#ip dhcp pool DMZ_CUsers
* Router(dhcp-config)#network 10.166.197.1 255.255.255.0
* Router(dhcp-config)#default-router 10.166.197.252
* Router(dhcp-config)#domain-name building-c.rcomp-19-20-df-g2
* Router(dhcp-config)#dns-server 10.166.197.252

###Building C2:
######### Excluir os ips ja atribuidos:
* Router(config)#ip dhcp excluded-address 10.166.200.2
* Router(config)#ip dhcp excluded-address 10.166.200.66
* Router(config)#ip dhcp excluded-address 10.166.200.130
* Router(config)#ip dhcp excluded-address 10.166.201.1
------------------------------------------------------------
* ip dhcp pool GroundFloorC2Users (vlan 320)
* Router(dhcp-config)#network 10.166.196.1 255.255.255.192
* Router(dhcp-config)#default-router 10.166.196.41
* Router(dhcp-config)#domain-name building-c2.rcomp-19-20-df-g2
* Router(dhcp-config)#dns-server 10.166.201.2
------------------------------------------------------------
* Router(config)#ip dhcp pool FirstFloorC2Users (vlan 321)
* Router(dhcp-config)#network 10.166.200.66 255.255.255.192
* Router(dhcp-config)#default-router 10.166.200.111
* Router(dhcp-config)#domain-name building-c2.rcomp-19-20-df-g2
* Router(dhcp-config)#dns-server 10.166.201.2
------------------------------------------------------------
* Router(config)#ip dhcp pool WiFiC2Users (vlan 322)
* Router(dhcp-config)#network 10.166.200.130 255.255.255.192
* Router(dhcp-config)#default-router 10.166.200.191
* Router(dhcp-config)#domain-name building-c2.rcomp-19-20-df-g2
* Router(dhcp-config)#dns-server 10.166.201.2
------------------------------------------------------------
* Router(config)#ip dhcp pool DMZ_C2Users
* Router(dhcp-config)#network 10.166.201.1 255.255.255.0
* Router(dhcp-config)#default-router 10.166.201.252
* Router(dhcp-config)#domain-name building-c2.rcomp-19-20-df-g2
* Router(dhcp-config)#dns-server 10.166.201.2
------------------------------------------------------------

* Router(config)#ip dhcp pool DMZ_C2Users
* Router(dhcp-config)#network 10.166.201.1 255.255.255.0
* Router(dhcp-config)#default-router 10.166.201.252
* Router(dhcp-config)#domain-name building-c2.rcomp-19-20-df-g2
* Router(dhcp-config)#dns-server 10.166.201.2

# Step 4: VoIP service

###Building A:




###Building B:




###Building C1:  



###Building C2:
