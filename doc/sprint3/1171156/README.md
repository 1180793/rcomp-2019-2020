﻿RCOMP 2019-2020 Project - Sprint 3 - All Members folder
==========================================================================================================================================================================================

*Step 1:* OSPF

**Building A:**: (Area 0)
# router ospf 400
# network ip mask area 0

**Building B:**: (Area 1)
# router ospf 500
# network ip mask area 0
# network ip mask area 1

**Building C1:**: (Area 2) MC Furtas
# router ospf 600
# network ip mask area 0
# network ip mask area 2

**Building C2:**: (Area 3) Gilaço
# router ospf 700
# network ip mask area 0
# network ip mask area 3

*Step 2:* Creating DMZ servers for each Building

**Building A:**
# HTTPS server IP:

**Building B:**
# HTTPS server IP:

**Building C1:**  
# HTTPS server IP:

**Building C2:**
# HTTPS server IP:

*Step 3:* DHCPv4service





*Step 4:* VoIP service







*Step 5:* DNS

Building A:
# Local DNS domain: building-A.rcomp-19-20-DF-02

Building B:
# Local DNS domain: building-B.rcomp-19-20-DF-02

Building C1:  
# Local DNS domain: building-C1.rcomp-19-20-DF-02

Building C2:
# Local DNS domain: building-C2.rcomp-19-20-DF-02
