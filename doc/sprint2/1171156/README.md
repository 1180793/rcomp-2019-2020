RCOMP 2019-2020 Project - Sprint 2 - Member 1171156 folder
=======================================================================================================================================================================================================


#### Inventário ####
MC............Procedeu-se á definição das VLANs no IC, sendo que este opera em modo servidor enquanto que o HC se encontra em modo cliente. Deste modo, todas as VLANs nos switches em modo Cliente
provêem do switch Servidor, ou seja do IC do edifício. O router, por sua vez, é responsável pela distribuição dos pacotes de informação pelas diferentes redes. 

### Decisões tomadads ##
Entre cada um dos Switches aplicaram-se ligações duplas de modo a permitir que, independentemente da falha de uma das ligações, a conexão não será perdida e as mensagens chegarão ao seu destino pelo
cabo de Fibra Ótica alterantivo. 

## Endereçamento da Rede ##
No Edifício A o endereçamento da rede está definido no intervalo 10.166.192.1 a 10.166.193.72 segundo a tabela seguinte:

| **End Devices** | **Nodes** |    **IP**     |   **Gateway**   |    **Mask**     | **Prefixo da Rede** |          **VLAN**          |
|:---------------:|:---------:|:-------------:|:---------------:|:---------------:|:-------------------:|:--------------------------:|
| End User Floor 0|    40     | 10.166.192.80 | 10.166.192.122  | 255.255.255.192 |         /26         |    311(FirstFloorAUsers)   |
| End User Floor 1|    40     | 10.166.192.1  | 10.166.192.42   | 255.255.255.192 |         /26         |    310(GroundFloorAUsers)  |
|  Wi-Fi Network  |    24     | 10.166.192.210| 10.166.192.235  | 255.255.255.224 |         /27         |    312(WiFiAUsers)         |
|Server+Admin(DMZ)|    70     |	10.166.193.1  | 10.166.193.72   | 255.255.255.128 |         /25         |    313(VoIP-AUsers)        |
|	  BackBone    |           |               |                 |                 |         /25         |                            |
