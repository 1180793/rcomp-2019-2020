RCOMP 2019-2020 Project - Sprint 2 - Member 1180837/1180838 folder

===========================================

**Inventário**:

Para o Edifício C as Vlans foram definidas no switch IC que é um switch em modo Servidor e os restantes switches (HCs e CPs) encontram-se em modo Cliente. A partir daí todas as Vlans nos switches em modo Cliente provêem do switch Servidor, ou seja do IC do edifício.

O router faz a distribuição de todas as redes necessárias para as respetivas Vlans logo este encontra-se ligado ao Intermediate Cross-Connect.

No Piso 0, para além do IC, HC (Horizontal Cross-Connect) e APs (Acess Points) definidos no Trabalho do Sprint 1, existe também 1 Server (DMZ_VLAN), 1 PC (GroundFloorUser_VLAN) e foram adicionados 2 CPs relativamente ao Sprint 1.

No Piso 1, para além do implementado no Sprint 1, existem 1 PC (FirstFloorCUsers_VLAN), 1 VoIP () e 1 Laptop Wireless () com capacidade para se conetar a qualquer AP do edifício.
 
**Decisões tomadas:**

Foi utilizada uma Topologia que aparenta um triângulo de forma a que a spanning tree funcione, ou seja, de forma a que no caso de uma ligação falhar, haver sempre mais uma alternativa de as mensagens chegarem ao seu destino.

No caso do Piso 1, como só existe uma coneção do HC ao CP foi utilizada uma dupla ligação entre os mesmos de forma a haver uma alternativa de transmissão caso a ligação falhe.

**Endereçamento da Rede:**

No Edifício C o endereçamento da rede está definido no intervalo 10.166.196.1 a 10.166.197.252 segundo a tabela seguinte:

|                End Devices                |       IP       |    Gateway     |   Subnet Mask   | Prefixo da Rede |        Vlan            |
| :---------------------------------------: | :------------: | :------------: | :-------------: | :-------------: | :--------------------: |
|        End User Piso 0 (40 nodes)         |  10.166.196.1  | 10.166.196.41  | 255.255.255.192 |       /26       | 346 (GrounfFloorCUsers)|
|        End User Piso 1 (44 nodes)         | 10.166.196.66  | 10.166.196.110 | 255.255.255.192 |       /26       | 345 (FirstFloorCUsers) |
| Wireless Laptop (WiFi Network) (60 nodes) | 10.166.196.129 | 10.166.196.190 | 255.255.255.192 |       /26       |  347 (WiFiCUsers)      |
|         Server (DMZ) (250 nodes)          |  10.166.197.1  | 10.166.197.252 |  255.255.255.0  |       /24       |  348 (DMZ_CUsers)      |