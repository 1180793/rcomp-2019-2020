RCOMP 2019-2020 Project - Sprint 2 planning
===========================================
### Sprint master: 1180737 ###

# 1. Sprint's backlog #
Development of a layer two Packet Tracer simulation for buildings A,B,C and also encompassing the campus backbone.

# 2. Technical decisions and coordination #
  * Virtual LANs
  * Spanning Tree Protocol (STP)
  * VLAN Trucking Protocol (VTP)
  * End Devices
  * Teamwork Organization
  * Packet Tracer Version 7.3.0
  * VTP Servers
  * Devices Naming
  * VLAN Database
  * VTP Domain = **vtdfg2**

# Device Naming (Building:N & Number:num)#

  * **Switches** : (MC|IC|HZ|CP)-N-num
  * **Access points** : AP-N-num
  * **PC** : PC-N-num
  * **Wireless Laptops** : WL-N-num
  * **Servers** : S-N-num
  * **VoIP Phones** : VoIP-N-num   
  * **Routers** : R-N-num

# VLAN's IDs and Names#

#### 1171156 - Building A (310 - 313) #
  * 310 : FirstFloorAUsers
  * 311 : GroundFloorAUsers
  * 312 : Wifi-AUsers
  * 313 : DMZ-AUsers
  * 314 : VoIP-AUsers


#### 1180793 - Building B (335 - 339) ##
  * 335 : GroundFloorBUsers
  * 336 : FirstFloorBUsers
  * 337 : Wifi-BUsers
  * 338 : DMZ-BUsers
  * 339 : VoIP-BUsers


####  1180837/1180838 - Building C (345 - 349)
  * 345 : FirstFloorCUsers  
  * 346 : GroundFloorCUsers
  * 347 : WiFiCUsers
  * 348 : DMZ_CUsers
  * 349 : VoIP_CUsers  

# 3. Subtasks assignment #
  *  VTP domain name to be used - **vtdfg2**.
  * 1171156 - Development of a layer two Packet Tracer simulation for building A, and also encompassing the campus backbone. Integration of every members Packet Tracer simulations into a single simulation.
  * 1180793 - Development of a layer two Packet Tracer simulation for building B, and also encompassing the campus backbone.
  * 1180837 - Development of a layer two Packet Tracer simulation for building C, and also encompassing the campus backbone.
  * 1180838 - Development of a layer two Packet Tracer simulation for building C, and also encompassing the campus backbone.
