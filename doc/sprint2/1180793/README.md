RCOMP 2019-2020 Project - Sprint 2 - Member 1180793 folder

===========================================

**Inventário**:

Para o Edifício B as Vlans foram definidas no switch IC que é um switch em modo Servidor e os restantes switches (HCs e CPs) encontram-se em modo Cliente. A partir daí todas as Vlans nos switches em modo Cliente provêm do switch Servidor(IC) através de interconnections em trunk-mode.
O router faz a distribuição de todas as redes necessárias para as respetivas VLANS logo este encontra-se ligado ao Intermediate Cross-Connect.
No Piso 0, para além do IC, HC (Horizontal Cross-Connect) e APs (Acess Points) definidos no Trabalho do Sprint 1, existe também 1 Server (DMZ_VLAN), 1 PC (GroundFloorBUser_VLAN) e 1 VoIP (VoIP_Users).
No Piso 1, para além do implementado no Sprint 1, existem 1 PC (FirstFloorBUsers_VLAN) e 1 Laptop Wireless com capacidade para se conetar a qualquer AP do edifício.

**Decisões tomadas:**

Foi utilizada uma Topologia que aparenta um triângulo de forma a que a spanning tree funcione, ou seja, de forma a que no caso de uma ligação falhar, haver sempre mais uma alternativa de as mensagens chegarem ao seu destino.

**Endereçamento da Rede:**

No Edifício B o endereçamento da rede está definido no intervalo 10.166.194.1 a 10.166.195.255 segundo a tabela seguinte:

|                End Devices                 |       IP         |    Gateway     |    Subnet Mask   | Prefixo da Rede |        VLANS           |
| :----------------------------------------: | :--------------: | :------------: | :--------------: | :-------------: | :--------------------: |
|      End User Ground Floor (60 nodes)      |  10.166.194.1    | 10.166.194.62  | 255.255.255.192  |       /26       | 335 (GroundFloorBUsers)|
|      End User First Floor (70 nodes)       |  10.166.194.75   | 10.166.194.146 | 255.255.255.128  |       /25       | 336 (FirstFloorBUsers) |
| Wireless Laptop (WiFi Network) (100 nodes) |  10.166.195.1    | 10.166.195.102 | 255.255.255.128  |       /25       | 337 (WiFiBUsers)       |
|         Server (DMZ) (12 nodes)            |  10.166.195.122  | 10.166.195.133 | 255.255.255.240  |       /28       | 338 (DMZ_BUsers)       |
