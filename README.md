RCOMP 2019-2020 Project repository template
===========================================
# 1. Team members (update this information please) #
  * 1180837 - Vasco Furtado
  * 1180793 - Gon�alo Corte Real
  * 1171156 - David Monteiro
  * 1180838 - Gil Pereira


Any team membership changes should be reported here:

-Member 1180838 (Gil Pereira) has entered the team on 2020-03-24


# 2. Sprints #
  * [Sprint 1](doc/sprint1/)
  * [Sprint 2](doc/sprint2/)
  * [Sprint 3](doc/sprint3/)
